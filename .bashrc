#
# ~/.bashrc
#

export LANG=en_US.UTF-8
export LC_MESSAGES="C"

PATH=$HOME/bin:$PATH

#TERM=xterm-256color

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias la='ls -a'
alias l='ls -l'
alias rm='rm -v'
alias cp='cp -v'
alias grep='grep --color'
alias acpi='acpi -V'
alias vstartx='startx & vlock'
alias tmux='TERM=xterm-256color tmux'
