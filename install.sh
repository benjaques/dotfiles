#!/bin/sh

export BASEDIR=$(dirname $0)

echo "Copying files into home directory..."
cp -r $BASEDIR/. $HOME

echo "Removing excess files from home directory..."
rm -rf $HOME/.git
rm $HOME/install.sh
rm $HOME/README.md
